import numpy as np

def fn(n):
  return str(int(n))

def main():
  n, m = list(map(int, input().split()))
  c = np.array(list(map(float, input().split())))
  N = np.array([list(map(float, input().split())) for _ in range(n)])

  A = np.zeros((n + m - 2, m))
  A[:n - 2, :] = N[1:-1]
  A[n - 2:, :] = np.eye(m)

  B = np.zeros(n + m - 2)
  B[n - 2:] = c

  C = np.zeros(m)
  C[np.where(N[0, :] != 0)] = 1

  tableau = np.zeros((n + m - 1, 2 * m + n - 1))
  tableau[0, :m] = -C
  tableau[1:, :m] = A
  tableau[1:, -1] = B
  tableau[1:, m:-1] = np.eye(m + n - 2)
  tableau = np.around(tableau, decimals=8)

  def output_handler(result):
    if result["type"] == "otima":
      print(fn(result["cost"]))
      print(" ".join(map(fn, result["solution"])))
      print(1, " ".join(map(fn, result["certificate"][:n - 2])), 0)

  print(tableau)
  simplex(n + m - 2, m, tableau, output_handler)

if __name__ == '__main__':
  main()